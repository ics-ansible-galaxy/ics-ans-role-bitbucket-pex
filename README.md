ics-ans-role-bitbucket-pex
==========================

This role is **DEPRECATED**!
The bitbucket.pex script has been replaced by:
- py-webhook to update webhooks on bitbucket and gitlab
- ics-jenkins-shared-libraries (with a curl command) to update commit build status


Ansible role to install the Python cli to interact with Bitbucket API.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
bitbucket_pex_version: v0.3.2
bitbucket_pex_url: https://artifactory.esss.lu.se/artifactory/swi-pkg/pex/python-bitbucket-api/{{ bitbucket_pex_version }}/bitbucket.pex
bitbucket_pex_config: "{{ playbook_dir }}/config/bitbucket.yml"
bitbucket_pex_user: user
bitbucket_pex_group: group
bitbucket_pex_user_home: "/home/{{ bitbucket_pex_user }}"
```

Create a `config` directory under the playbook base directory to store your bitbucket.yml file
or set the `bitbucket_pex_config` variable to the full path of your configuration.
You can find a dummy bitbucket.yml file under config in this role.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-bitbucket-pex
```

License
-------

BSD 2-clause
